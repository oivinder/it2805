const contactForm =  document.getElementById("contactForm");

function changeDisplay () {
    const email = document.forms[0][1].value;

    if (validateEmail(email)) {
        contactForm.innerText = "Takk for din melding. Vi tar kontakt i løpet av 2 virkedager.";
        send.style.display = "none"
        return;
    }

    document.getElementById("emailError").style.display = "inline"
}

function validateEmail (email) {
    return email
    .toLowerCase()
    // RegExp lånt fra https://regexr.com/3e48o og modifisert
    .match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,}$/);
}
