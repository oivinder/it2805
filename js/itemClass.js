class item{
    constructor(id, name, price, png, alt){
        this.id = id;
        this.name = name;
        this.price = price;
        this.png = "./img/" + png;
        this.alt = alt;
    }

    setCount(n){
        localStorage.setItem(this.id, n);
    }

    getCount(){
        return Number(localStorage.getItem(this.id)) || 0;
    }

    add(){
        this.setCount(Number(this.getCount()) + 1);
    }
}

const items = [
    new item("gulrot", "Gulrøtter", 25, "carrots.png", "fotografi av gulrot"),
    new item("agurk", "Agurk", 30, "agurk.jpg", "dataart av agurk"),
    new item("salat", "Salat", 50, "salat.jpg", "fotografi av salat"),
    new item("lammelaar", "Lammelår", 150, "lam.png", "grafikk av et lam"),
    new item("indrefilet", "Indrefilet av svin", 200, "svin.png", "grafikk av en gris"),
    new item("ribbe", "Juleribbe", 350, "svin.png", "grafikk av en gris"),
    new item("fetaost", "Fetaost av geitemelk", 90, "feta.jpeg", "fotografi av fetaost"),
    new item("mozzarella", "Fersk mozzarella av kumelk", 75, "mozzarella.jpeg", "fotografi av mozzarella"),
    new item("brunost", "Brunost", 40, "brunost.jpeg", "fotografi av brunost")
];