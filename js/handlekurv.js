//Setup for the back button
let referrer = document.referrer;

if(referrer){
    document.getElementById("backButton").href = referrer;
}

let noscript = document.getElementById("noScript");
noscript.parentNode.removeChild(noscript);

//added contains all items that have a count of > 0
let added = items.filter(i => i.getCount() > 0)
let table = document.querySelector("table");

//Creates a table row and appends it to the table
const addElement = function(id, icon, name, count, price, alt){
    let row = document.createElement("tr");
    row.setAttribute("id", id)

    let itemIcon = document.createElement("td");
    itemIcon.innerHTML = "<img src='" + icon + "' alt='" + alt + "'>";
    row.appendChild(itemIcon);

    let itemName = document.createElement("td");
    itemName.innerHTML = name;
    row.appendChild(itemName);

    let itemCount = document.createElement("td");
    itemCount.innerHTML = "<input class='itemCount' type='number' min='0' value='" + count + "'>";
    row.appendChild(itemCount);

    let itemPrice = document.createElement("td");
    itemPrice.innerHTML = price + ",-";
    row.appendChild(itemPrice);
    
    let itemSum = document.createElement("td");
    itemSum.innerHTML = price * count + ",-";
    row.appendChild(itemSum);

    table.appendChild(row);
}

const calculateSum = function(){
    let sum = 0;

    for(let item of added){
        sum += item.getCount() * item.price;
    }

    return sum;
}

//If added has no items then the shopping cart is empty
if(added.length === 0){
    let warning = "<td colspan='5'>Ingen varer har blitt lagt til handlekurven</td>";
    let element = document.createElement("tr");
    element.innerHTML = warning;
    table.appendChild(element);
}
else {
    //Iterate over every item in the shopping cart, and create a table row for each
    for(let item of added){
        addElement(item.id, item.png, item.name, item.getCount(), item.price, item.alt);
    }

    document.getElementById("totalCost").innerText = calculateSum() + ",-";
}

//Add eventlisteners to input tags
const updatePrices = function(){
    let parent = this.parentNode.parentNode;
    let item = items.filter(i => i.id === parent.id)[0];

    if(this.value === "0"){
        parent.remove();
        item.setCount(0);

        //If there are no items in the shopping cart now, add the warning back in.
        let elements = document.querySelectorAll("tr");

        if(elements.length === 1){
            let warning = "<td colspan='5'>Du har ingen varer i handlekurven</td>";
            let element = document.createElement("tr");
            element.innerHTML = warning;
            table.appendChild(element);
        }
    }
    else {
        item.setCount(this.value);
        parent.lastChild.innerHTML = (item.getCount() * item.price) + ",-";
    }
    
    document.getElementById("totalCost").innerText = calculateSum() + ",-";

    updateCounter();
}

let inputs = document.querySelectorAll("table input");

for(let input of inputs){
    input.addEventListener("input", updatePrices);
}

//buy button should give a confirmation
document.getElementById("finishTransaction").addEventListener("click", function(){
    document.getElementById("handlekurv").innerHTML = "Takk for kjøpet";

    localStorage.clear();
});