const alignPupil = (p, r) => {
  let w = document.querySelector(".eye").clientWidth * 0.40;

  let x = Math.cos(r) * 5 + "px";
  let y = Math.sin(r) * 5 + "px";

  p.style.transform = "translate(" + x + "," + y + ")";
};

const followPointer =  (event) => {
  document.querySelectorAll(".pupil").forEach(p => {
    let rect = p.getBoundingClientRect();
    let r = Math.atan2(
      event.pageY - rect.y,
      event.pageX - rect.x,
    );

    alignPupil(p, r);
  });
};

document.addEventListener("mousemove", followPointer);
document.addEventListener("mousemove", followPointer);
