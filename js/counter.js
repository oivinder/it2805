const countItems = () => {
    return items
    .map(i => i.getCount())
    .reduce((a, b) => a + b, 0);
}

const updateCounter = () => {
    let count = countItems();
    const counter = document.querySelector("#count");
    counter.innerText = count;
}

updateCounter();