
let navLinks = document.querySelectorAll("#nav > a");

scrollFunction();

window.onscroll = function() {scrollFunction()};
function scrollFunction() {
    if (window.scrollY > 20) {
        document.getElementById("nav").style.height = "45px";
        document.getElementById("banner").style.height = 35+"vh";

        for (let link of navLinks){
            link.style.padding = "0";
        }

    } else {
        let scrollAmount = 20 - window.scrollY;

        document.getElementById("nav").style.height = 45+scrollAmount+"px";

        let max = document.documentElement.scrollHeight - document.documentElement.clientHeight;

        document.getElementById("banner").style.height = max > 20 ? (35+(scrollAmount/2)+"vh") : (35+"vh");
        document.getElementById("counter").style.transform = "translateY(" + (100 + scrollAmount * 2.3) + "%) translateX(60%)";

        for (let link of navLinks){
            link.style.paddingTop = (scrollAmount/2)+"px";            
        }
    }
}